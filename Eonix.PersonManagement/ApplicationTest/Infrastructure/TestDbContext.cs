﻿using Application.Interfaces;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace ApplicationTest.Infrastructure
{
    internal class TestDbContext : DbContext, IPersonManagementDbContext
    {
        public TestDbContext(DbContextOptions options) : base(options)
        {

        }

        public DbSet<Person> Persons { get; set; }


        public override void Dispose()
        {
            base.Dispose();
        }

    }
}
