﻿using Application.Persons.Commands.CreatePerson;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ApplicationTest.Commands.CreatePerson
{
    [TestClass]
    public class CreatePersonCommandRequestValidatorTest
    {
        [TestMethod]
        [DataRow("Test", "Test", true)]
        [DataRow("", "Test", false)]
        [DataRow("test", "", false)]
        [DataRow("", "", false)]
        public void Validate_ThenIsEqualsToExpected(string firstName, string lastName, bool expected)
        {
            //Arrange
            var validator = new CreatePersonCommandRequestValidator();
            var request = new CreatePersonCommandRequest()
            {
                FirstName = firstName,
                LastName = lastName
            };

            //Act
            var result = validator.Validate(request);

            //Assert
            result.IsValid.Should().Be(expected);
        }
    }
}
