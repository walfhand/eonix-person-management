﻿using Application.Interfaces;
using Application.Persons;
using Application.Persons.Commands.CreatePerson;
using AutoMapper;
using Domain.Entities;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace ApplicationTest.Commands.CreatePerson
{
    [TestClass]
    public class CreatePersonCommandHandlerTest
    {
        private readonly IPersonManagementDbContext _context;
        private readonly IMapper _mapper;
        public CreatePersonCommandHandlerTest()
        {
            IServiceCollection services = DependencyInjection.AddApplicationTest();
            _context = services.BuildServiceProvider().GetService<IPersonManagementDbContext>();
            _mapper = services.BuildServiceProvider().GetService<IMapper>();
        }

        [TestCleanup]
        public void Clean()
        {
            _context.Dispose();
        }

        [TestMethod]
        public async Task Handle_Then_NewPersonIsCreated()
        {
            //Arrange
            var request = new CreatePersonCommandRequest()
            {
                FirstName = "Person1FirstName",
                LastName = "Person1LastName"
            };
            var handler = new CreatePersonCommandRequestHandler(_context, _mapper);

            //Act
            PersonDto personDto = await handler.Handle(request, default);

            //Assert
            personDto.Should().NotBeNull();
            personDto.Id.Should().NotBeEmpty();
            Person person = await _context.Persons.FindAsync(personDto.Id);
            person.Should().NotBeNull();
            person.Id.Should().Be(personDto.Id);
        }
    }
}
