﻿using Application.Persons.Commands.UpdatePerson;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace ApplicationTest.Commands.UpdatePerson
{
    [TestClass]
    public class UpdatePersonCommandRequestValidatorTest
    {
        [TestMethod]
        [DataRow("test", "test", true)]
        [DataRow("test", "", false)]
        [DataRow("", "test", false)]
        [DataRow("", "", false)]
        public void Validate_ThenIsEqualsToExpected(string firstname, string lastname, bool expected)
        {
            //Arrange
            var validator = new UpdatePersonCommandRequestValidator();
            var request = new UpdatePersonCommandRequest()
            {
                Firstname = firstname,
                LastName = lastname,
            };

            //Act
            var result = validator.Validate(request);

            //Assert
            result.IsValid.Should().Be(expected);
        }
    }
}
