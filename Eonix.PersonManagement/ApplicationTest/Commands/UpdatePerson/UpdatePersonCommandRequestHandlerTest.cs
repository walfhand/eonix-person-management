﻿using Application.Interfaces;
using Application.Persons.Commands.UpdatePerson;
using Common.Exceptions;
using Domain.Entities;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;

namespace ApplicationTest.Commands.UpdatePerson
{
    [TestClass]
    public class UpdatePersonCommandRequestHandlerTest
    {
        private readonly IPersonManagementDbContext _context;
        private Person _person;
        public UpdatePersonCommandRequestHandlerTest()
        {
            IServiceCollection services = DependencyInjection.AddApplicationTest();
            _context = services.BuildServiceProvider().GetService<IPersonManagementDbContext>();
        }

        [TestInitialize]
        public async Task Initialize()
        {
            _context.Persons.RemoveRange(_context.Persons);
            _person = Person.Create("testFirstName", "TestLastname");
            _context.Persons.Add(_person);
            await _context.SaveChangesAsync(default);
        }

        [TestCleanup]
        public void Clean()
        {
            _context.Dispose();
        }

        [TestMethod]
        public async Task Handle_Then_PersonWasUpdated()
        {
            //Arrange

            var request = new UpdatePersonCommandRequest
            {
                Firstname = "test2FirstName",
                LastName = "Test2LastName",
                Id = _person.Id
            };

            var handler = new UpdatePersonCommandRequestHandler(_context);

            //Act
            await handler.Handle(request, default);

            //assert
            _person.FirstName.Should().Be(request.Firstname);
            _person.LastName.Should().Be(request.LastName);
            _person.Id.Should().Be(request.Id);
        }

        [TestMethod]
        public void Handle_WhenGivenBadId_ThenNotFoundException()
        {
            //Arrange
            var request = new UpdatePersonCommandRequest
            {
                Firstname = "testFirstName",
                LastName = "TestLastName",
                Id = Guid.NewGuid()
            };

            var handler = new UpdatePersonCommandRequestHandler(_context);

            //Act
            Func<Task> act = async () => await handler.Handle(request, default);

            //Assert
            act.Should().Throw<NotFoundException>();
        }
    }
}
