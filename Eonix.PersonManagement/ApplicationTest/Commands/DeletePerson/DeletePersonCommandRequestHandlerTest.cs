﻿using Application.Interfaces;
using Application.Persons.Commands.DeletePerson;
using Common.Exceptions;
using Domain.Entities;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;

namespace ApplicationTest.Commands.DeletePerson
{
    [TestClass]
    public class DeletePersonCommandRequestHandlerTest
    {
        private readonly IPersonManagementDbContext _context;
        private Person _person;

        public DeletePersonCommandRequestHandlerTest()
        {
            IServiceCollection services = DependencyInjection.AddApplicationTest();
            _context = services.BuildServiceProvider().GetService<IPersonManagementDbContext>();
        }

        [TestInitialize]
        public void Initialize()
        {
            _context.Persons.RemoveRange(_context.Persons);
            _person = Person.Create("testFirstName", "TestLastname");
            _context.Persons.Add(_person);
            _context.SaveChangesAsync(default);
        }

        [TestCleanup]
        public void Clean()
        {
            _context.Dispose();
        }

        [TestMethod]
        public async Task Handle_Then_PersonWasDelete()
        {
            //Arrange
            var request = new DeletePersonCommandRequest()
            {
                Id = _person.Id
            };
            var handler = new DeletePersonCommandRequestHandler(_context);

            //Act
            await handler.Handle(request, default);
            var personAfterDeleted = await _context.Persons.FindAsync(request.Id);

            //Assert
            personAfterDeleted.Should().BeNull();
        }

        [TestMethod]
        public void Handle_WhenGivenBadId_ThenNotFoundException()
        {
            //Arrange
            var request = new DeletePersonCommandRequest()
            {
                Id = Guid.NewGuid()
            };
            var handler = new DeletePersonCommandRequestHandler(_context);

            //Act
            Func<Task> act = async () => await handler.Handle(request, default);

            //assert
            act.Should().Throw<NotFoundException>();
        }
    }
}
