﻿using Application.Interfaces;
using Application.Mappings;
using ApplicationTest.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace ApplicationTest
{
    public static class DependencyInjection
    {
        private static IServiceCollection _services;
        public static IServiceCollection AddApplicationTest()
        {
            if (_services != null)
            {
                return _services;
            }
            _services = new ServiceCollection();
            _services.AddDbContext<TestDbContext>(options => options.UseInMemoryDatabase("PersonManagementDb"));
            _services.AddScoped<IPersonManagementDbContext>(provider => provider.GetService<TestDbContext>());
            _services.BuildServiceProvider().GetRequiredService<TestDbContext>().Database.EnsureDeleted();
            _services.BuildServiceProvider().GetRequiredService<TestDbContext>().Database.EnsureCreated();
            _services.AddAutoMapper(typeof(PersonProfile).Assembly);
            return _services;
        }
    }
}
