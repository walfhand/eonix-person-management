﻿using Application.Interfaces;
using Application.Persons;
using Application.Persons.Queries.GetPerson;
using AutoMapper;
using Common.Exceptions;
using Domain.Entities;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;

namespace ApplicationTest.Queries.GetPerson
{
    [TestClass]
    public class GetPersonQueryHandlerTest
    {
        private readonly IPersonManagementDbContext _context;
        private readonly IMapper _mapper;
        private Person _person;
        public GetPersonQueryHandlerTest()
        {
            IServiceCollection services = DependencyInjection.AddApplicationTest();
            _context = services.BuildServiceProvider().GetService<IPersonManagementDbContext>();
            _mapper = services.BuildServiceProvider().GetService<IMapper>();
        }

        [TestInitialize]
        public async Task Initialize()
        {
            _context.Persons.RemoveRange(_context.Persons);
            _person = Person.Create("testFirstname", "TestLastname");
            _context.Persons.Add(_person);
            await _context.SaveChangesAsync(default);
        }

        [TestMethod]
        public async Task Handle_Then_PersonFound()
        {
            //Arrange
            var query = new GetPersonQuery()
            {
                Id = _person.Id
            };
            var handler = new GetPersonQueryHandler(_context, _mapper);

            //Act
            PersonDto personDto = await handler.Handle(query, default);
            //Assert
            personDto.Should().NotBeNull();
            personDto.Id.Should().Be(query.Id);
        }


        [TestMethod]
        public void Handle_WhenGivenBadId_ThenNotFoundException()
        {
            //Arrange
            var query = new GetPersonQuery()
            {
                Id = Guid.NewGuid()
            };
            var handler = new GetPersonQueryHandler(_context, _mapper);

            //Act
            Func<Task> act = async () => await handler.Handle(query, default);
            //Assert
            act.Should().Throw<NotFoundException>();
        }

        [TestCleanup]
        public void Clean()
        {
            _context.Dispose();
        }
    }
}
