﻿using Application.Interfaces;
using Application.Persons;
using Application.Persons.Queries.FilterPerson;
using AutoMapper;
using Domain.Entities;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApplicationTest.Queries.FilterPerson
{
    [TestClass]
    public class FilterPersonQueryHandlerTest
    {
        private readonly IPersonManagementDbContext _context;
        private readonly IMapper _mapper;
        private Person _person1;
        private Person _person2;
        private Person _person3;

        public FilterPersonQueryHandlerTest()
        {
            IServiceCollection services = DependencyInjection.AddApplicationTest();
            _context = services.BuildServiceProvider().GetService<IPersonManagementDbContext>();
            _mapper = services.BuildServiceProvider().GetService<IMapper>();
        }


        [TestInitialize]
        public async Task Initialize()
        {
            _context.Persons.RemoveRange(_context.Persons);
            _person1 = Person.Create("Jean", "Dupont");
            _person2 = Person.Create("Sébastien", "Vandaele");
            _person3 = Person.Create("Pierre", "Qui roule");

            _context.Persons.AddRange(_person1, _person2, _person3);
            await _context.SaveChangesAsync(default);
        }

        [TestCleanup]
        public void Clean()
        {
            _context.Dispose();
        }

        [TestMethod]
        [DataRow("", "", 3)]
        [DataRow("sé", "", 1)]
        [DataRow("", "le", 2)]
        [DataRow("pi", "le", 1)]
        [DataRow("as", "", 0)]
        public async Task Handle_ThenIsEqualsToExpected(string firstname, string lastName, int expected)
        {
            //Arrange
            var query = new FilterPersonQuery()
            {
                Firstname = firstname,
                LastName = lastName,
            };

            var handler = new FilterPersonQueryHandler(_context, _mapper);

            //Act
            List<PersonDto> result = await handler.Handle(query, default);

            //Assert
            result.Should().NotBeNull();
            result.Count.Should().Be(expected);

        }
    }
}
