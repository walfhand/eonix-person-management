﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models
{
    public class ValidationResponse
    {
        public string Title { get; set; }
        public int Code { get; set; }
        public List<string> Errors { get; set; }
    }
}
