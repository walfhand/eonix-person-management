﻿using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Exceptions
{
    public class ValidationException : Exception
    {
        public List<ValidationFailure> ValidationFailures { get;}
        public ValidationException(List<ValidationFailure> failures) : base("One or more validation failures have occurred.")
        {
            ValidationFailures = failures;
        }
    }
}
