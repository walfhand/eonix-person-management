﻿using System;

namespace Domain.Entities
{
    public class Person
    {
        public Guid Id { get; private set; }
        public string FirstName { get; private set; }
        public string LastName { get; private set; }

        public static Person Create(string firstName, string lastName)
        {
            return new Person
            {
                FirstName = firstName,
                LastName = lastName,
            };
        }

        public void Update(string firstName, string lastname)
        {
            FirstName = firstName;
            LastName = lastname;
        }
    }
}
