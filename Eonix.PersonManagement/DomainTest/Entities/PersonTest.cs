﻿using Domain.Entities;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DomainTest.Entities
{
    [TestClass]
    public class PersonTest
    {
        [TestMethod]
        public void CreateThenUserIsCreated()
        {
            //Act
            var person = Person.Create("testFirstname", "TestLastname");

            //Assert
            person.Should().NotBeNull();
            person.FirstName.Should().Be("testFirstname");
            person.LastName.Should().Be("TestLastname");
        }


        [TestMethod]
        public void UpdateThenUserIsUpdated()
        {
            //Arrange
            var person = Person.Create("testFirstname", "TestLastname");

            //Act
            person.Update("testFirstnameUpdate", "TestLastnameUpdate");
            //Assert
            person.Should().NotBeNull();
            person.FirstName.Should().Be("testFirstnameUpdate");
            person.LastName.Should().Be("TestLastnameUpdate");
        }
    }
}
