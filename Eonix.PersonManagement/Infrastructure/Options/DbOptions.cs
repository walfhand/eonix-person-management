﻿namespace Infrastructure.Options
{
    internal class DbOptions
    {
        public const string ConfigurationName = "DbOptions";
        public bool UseInMemoryDatabase { get; set; }
        public string RootDirectorySqlLite { get; set; }
    }
}
