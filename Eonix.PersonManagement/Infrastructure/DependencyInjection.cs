﻿using Application.Interfaces;
using Infrastructure.Options;
using Infrastructure.Persistence;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.IO;

namespace Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            DbOptions dbOptions = new DbOptions();
            configuration.GetSection(DbOptions.ConfigurationName).Bind(dbOptions);

            if (dbOptions.UseInMemoryDatabase)
            {
                services.AddDbContext<PersonManagementDbContext>(options => options.UseInMemoryDatabase("PersonManagementDb"));
            }
            else
            {
                string connexionString = configuration.GetConnectionString("PersonManagementDb");
                services.AddDbContext<PersonManagementDbContext>(options => options.UseSqlite(connexionString));
                if (!Directory.Exists(dbOptions.RootDirectorySqlLite))
                {
                    Directory.CreateDirectory(dbOptions.RootDirectorySqlLite);
                }
            }

            services.AddScoped<IPersonManagementDbContext>(provider => provider.GetService<PersonManagementDbContext>());

            services.BuildServiceProvider().GetRequiredService<PersonManagementDbContext>().Database.EnsureCreated();
            return services;
        }
    }
}
