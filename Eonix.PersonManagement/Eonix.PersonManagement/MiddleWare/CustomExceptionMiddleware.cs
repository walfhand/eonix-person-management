﻿using Common.Exceptions;
using Common.Models;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Eonix.PersonManagement.MiddleWare
{
    public class CustomExceptionMiddleware
    {
        private readonly RequestDelegate _next;

        public CustomExceptionMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (NotFoundException)
            {
                httpContext.Response.StatusCode = (int)HttpStatusCode.NotFound;
            }
            catch(ValidationException e)
            {
                httpContext.Response.ContentType = "application/json";
                httpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                var validationResponse = new ValidationResponse()
                {
                    Code = httpContext.Response.StatusCode,
                    Title = e.Message,
                    Errors = e.ValidationFailures.Select(f => f.ErrorMessage).ToList()
                };
                string json = JsonConvert.SerializeObject(validationResponse, Formatting.Indented);
                await httpContext.Response.WriteAsync(json);
            }
        }
    }
}
