﻿using Application.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Persons.Queries.FilterPerson
{
    public class FilterPersonQueryHandler : IRequestHandler<FilterPersonQuery, List<PersonDto>>
    {
        private readonly IPersonManagementDbContext _context;
        private readonly IMapper _mapper;

        public FilterPersonQueryHandler(IPersonManagementDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<List<PersonDto>> Handle(FilterPersonQuery request, CancellationToken cancellationToken)
        {
            IQueryable<Person> query = _context.Persons;
            if (!string.IsNullOrEmpty(request.Firstname))
            {
                query = query.Where(p => p.FirstName.ToLower()
                .StartsWith(request.Firstname.ToLower()) || p.FirstName.ToLower().EndsWith(request.Firstname.ToLower()));
            }

            if (!string.IsNullOrEmpty(request.LastName))
            {
                query = query.Where(p => p.LastName.ToLower()
                .StartsWith(request.LastName.ToLower()) || p.LastName.ToLower().EndsWith(request.LastName.ToLower()));
            }

            return await query.ProjectTo<PersonDto>(_mapper.ConfigurationProvider).ToListAsync();
        }
    }
}
