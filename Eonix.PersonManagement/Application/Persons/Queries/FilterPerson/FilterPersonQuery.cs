﻿using MediatR;
using System.Collections.Generic;

namespace Application.Persons.Queries.FilterPerson
{
    public class FilterPersonQuery : IRequest<List<PersonDto>>
    {
        public string Firstname { get; set; }
        public string LastName { get; set; }
    }
}
