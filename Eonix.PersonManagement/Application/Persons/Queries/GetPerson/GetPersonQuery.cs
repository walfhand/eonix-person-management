﻿using MediatR;
using System;

namespace Application.Persons.Queries.GetPerson
{
    public class GetPersonQuery : IRequest<PersonDto>
    {
        public Guid Id { get; set; }
    }
}
