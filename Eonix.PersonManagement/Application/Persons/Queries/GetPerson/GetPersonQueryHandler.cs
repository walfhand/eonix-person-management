﻿using Application.Interfaces;
using AutoMapper;
using Common.Exceptions;
using Domain.Entities;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Persons.Queries.GetPerson
{
    public class GetPersonQueryHandler : IRequestHandler<GetPersonQuery, PersonDto>
    {
        private readonly IPersonManagementDbContext _context;
        private readonly IMapper _mapper;

        public GetPersonQueryHandler(IPersonManagementDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<PersonDto> Handle(GetPersonQuery request, CancellationToken cancellationToken)
        {
            Person person = await _context.Persons.FindAsync(request.Id);
            if (person == null)
                throw new NotFoundException();

            return _mapper.Map<PersonDto>(person);
        }
    }
}
