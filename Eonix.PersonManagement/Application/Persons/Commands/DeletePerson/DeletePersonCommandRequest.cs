﻿using MediatR;
using System;

namespace Application.Persons.Commands.DeletePerson
{
    public class DeletePersonCommandRequest : IRequest
    {
        public Guid Id { get; set; }
    }
}
