﻿using Application.Interfaces;
using Common.Exceptions;
using Domain.Entities;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Persons.Commands.DeletePerson
{
    public class DeletePersonCommandRequestHandler : IRequestHandler<DeletePersonCommandRequest>
    {
        private readonly IPersonManagementDbContext _context;

        public DeletePersonCommandRequestHandler(IPersonManagementDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(DeletePersonCommandRequest request, CancellationToken cancellationToken)
        {
            Person person = await _context.Persons.FindAsync(request.Id);
            if (person == null)
                throw new NotFoundException();
            _context.Persons.Remove(person);
            await _context.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}
