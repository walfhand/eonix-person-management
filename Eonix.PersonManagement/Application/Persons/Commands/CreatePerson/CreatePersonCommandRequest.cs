﻿using MediatR;

namespace Application.Persons.Commands.CreatePerson
{
    public class CreatePersonCommandRequest : IRequest<PersonDto>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
