﻿using FluentValidation;

namespace Application.Persons.Commands.CreatePerson
{
    public class CreatePersonCommandRequestValidator : AbstractValidator<CreatePersonCommandRequest>
    {
        public CreatePersonCommandRequestValidator()
        {
            RuleFor(p => p.FirstName)
                .NotEmpty();

            RuleFor(p => p.LastName)
                .NotEmpty();
        }
    }
}
