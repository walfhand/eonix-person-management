﻿using Application.Interfaces;
using AutoMapper;
using Domain.Entities;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Persons.Commands.CreatePerson
{
    public class CreatePersonCommandRequestHandler : IRequestHandler<CreatePersonCommandRequest, PersonDto>
    {
        private readonly IPersonManagementDbContext _context;
        private readonly IMapper _mapper;

        public CreatePersonCommandRequestHandler(IPersonManagementDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<PersonDto> Handle(CreatePersonCommandRequest request, CancellationToken cancellationToken)
        {
            var person = Person.Create(request.FirstName, request.LastName);
            _context.Persons.Add(person);
            await _context.SaveChangesAsync(cancellationToken);

            return _mapper.Map<PersonDto>(person);
        }
    }
}
