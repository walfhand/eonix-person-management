﻿using Application.Interfaces;
using Common.Exceptions;
using Domain.Entities;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Persons.Commands.UpdatePerson
{
    public class UpdatePersonCommandRequestHandler : IRequestHandler<UpdatePersonCommandRequest>
    {
        private readonly IPersonManagementDbContext _context;

        public UpdatePersonCommandRequestHandler(IPersonManagementDbContext context)
        {
            _context = context;
        }
        public async Task<Unit> Handle(UpdatePersonCommandRequest request, CancellationToken cancellationToken)
        {
            Person person = await _context.Persons.FindAsync(request.Id);
            if (person == null)
            {
                throw new NotFoundException();
            }
            person.Update(request.Firstname, request.LastName);
            await _context.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}
