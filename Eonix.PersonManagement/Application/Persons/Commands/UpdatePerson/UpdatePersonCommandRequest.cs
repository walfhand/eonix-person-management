﻿using MediatR;
using System;
using System.Text.Json.Serialization;

namespace Application.Persons.Commands.UpdatePerson
{
    public class UpdatePersonCommandRequest : IRequest
    {
        [JsonIgnore]
        public Guid Id { get; set; }
        public string Firstname { get; set; }
        public string LastName { get; set; }
    }
}
