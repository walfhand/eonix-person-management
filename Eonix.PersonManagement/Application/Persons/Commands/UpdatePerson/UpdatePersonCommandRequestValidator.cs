﻿using FluentValidation;

namespace Application.Persons.Commands.UpdatePerson
{
    public class UpdatePersonCommandRequestValidator : AbstractValidator<UpdatePersonCommandRequest>
    {
        public UpdatePersonCommandRequestValidator()
        {
            RuleFor(p => p.Firstname)
                .NotEmpty();
            RuleFor(p => p.LastName)
                .NotEmpty();
        }
    }
}
