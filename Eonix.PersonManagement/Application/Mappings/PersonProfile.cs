﻿using Application.Persons;
using AutoMapper;
using Domain.Entities;

namespace Application.Mappings
{
    public class PersonProfile : Profile
    {
        public PersonProfile()
        {
            CreateMap<Person, PersonDto>()
                .ReverseMap();
        }
    }
}
